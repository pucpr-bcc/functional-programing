module Main where

stair :: Int -> [String]
stair a = stairInt a a

stairInt :: Int -> Int -> [String]
stairInt _ 0 = []
stairInt a 1 = [replicate a '#']
stairInt a b = (replicate (b - 1) ' ' ++ replicate (a-b + 1) '#')  : stairInt a (b - 1)


main :: IO()
main = interact $ unlines . stair . read