module Main where

myFilter :: (Int->Bool) -> [Int] -> [Int]
myFilter _ [] = []
myFilter f (fst:o)
    | f fst = fst : myFilter f o
    | otherwise = myFilter f o

len :: [a] -> Int
len [] = 0
len (_:lst) = 1 + len lst

fat :: Int -> Int
fat 0 = 1
fat n = n * fat(n - 1)

ex :: Float -> Float
ex x = sum $ map (\y -> (x ^ y) / fromIntegral (fat y)) [0 .. 10]
    

f :: Int -> [Int] -> [Int]
f n = myFilter (>= n)

g :: [Int] -> [Int]
g n = map (n!!) [1,3..length n - 1]

rev :: [Int] -> [Int]
rev [] = []
rev l = rev (tail l) ++ [head l]

h :: [Int] -> Int
h = sum . filter odd

-- The Input/Output section. You do not need to change or modify this part
main = do 
    n <- readLn :: IO Int 
    inputdata <- getContents 
    let 
        numbers = map read (lines inputdata) :: [Int] 
    putStrLn . unlines $ (map show . f n) numbers