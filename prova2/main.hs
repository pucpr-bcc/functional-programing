module Main where

merge :: Ord a => [a] -> [a] -> [a]
merge a [] = a
merge [] b = b
merge a@(a1:as) b@(b1:bs) | a1 <= b1 = a1:merge as b
                          | otherwise = b1:merge a bs

mergeSort :: Ord a => [a] -> [a]
mergeSort [] = []
mergeSort [n] = [n]
mergeSort x = merge (mergeSort p1) (mergeSort p2)
    where
        p1 = take (div (length x) 2) x
        p2 = drop (div (length x) 2) x


data BTree a = Empty | Leaf a | Node (BTree a) a (BTree a)
    deriving (Show)

root :: BTree a -> a
root Empty = error "Empty tree"
root (Leaf a) = a
root (Node _ v _) = v

toLeaf :: BTree a -> BTree a
toLeaf Empty = Empty
toLeaf l@(Leaf a) = l
toLeaf (Node Empty v Empty) = Leaf v
toLeaf n = n

setRoot :: BTree a -> a -> BTree a
setRoot Empty _ = Empty
setRoot (Leaf v) a = Leaf a
setRoot (Node l v r) a = Node l a r

swapLeft :: Ord a => BTree a -> BTree a
swapLeft Empty = Empty
swapLeft l@(Leaf v) = l
swapLeft node@(Node Empty v r) = node
swapLeft node@(Node l v r)
    | root l <= v = node
    | otherwise = Node (setRoot l v) (root l) r

swapRight :: Ord a => BTree a -> BTree a
swapRight Empty = Empty
swapRight l@(Leaf v) = l
swapRight node@(Node l v Empty) = node
swapRight node@(Node l v r)
    | root r <= v = node
    | otherwise = Node l (root r) (setRoot r v)

moveRootUp :: Ord a => BTree a -> BTree a
moveRootUp Empty = Empty
moveRootUp l@(Leaf v) = l
moveRootUp n@(Node Empty v r) = swapRight n
moveRootUp n@(Node l v Empty) = swapLeft n
moveRootUp n@(Node l v r)
    | root l > root r = swapLeft n
    | otherwise = swapRight n

deleteRoot :: Ord a => BTree a -> BTree a
deleteRoot Empty = Empty
deleteRoot (Leaf _) = Empty
deleteRoot (Node Empty v r) = toLeaf . buildHeap $ Node Empty (root r) (deleteRoot (setRoot r v))
deleteRoot (Node l v Empty) = toLeaf . buildHeap $ Node (deleteRoot (setRoot l v)) (root l) Empty
deleteRoot (Node l v r)
    | root l >= root r = toLeaf . buildHeap $ Node (deleteRoot (setRoot l v)) (root l) r
    | otherwise = toLeaf . buildHeap $ Node l (root r) (deleteRoot (setRoot r v))

buildHeap :: Ord a => BTree a -> BTree a
buildHeap Empty = Empty
buildHeap l@(Leaf _) = l
buildHeap (Node l v r) = moveRootUp (Node (buildHeap l) v (buildHeap r))

toHeapBTree :: Ord a => [a] -> BTree a
toHeapBTree [] = Empty
toHeapBTree [a] = Leaf a
toHeapBTree x@(a:b:xs)
    | a >= b = buildHeap $ toLeaf $ Node (toHeapBTree (b:p1)) a (toHeapBTree p2)
    | otherwise = buildHeap $ toLeaf $ Node (toHeapBTree (a:p1)) b (toHeapBTree p2)
    where
        l = length xs
        p1 = take l xs
        p2 = drop l xs

heapsort :: Ord a => [a] -> [a]
heapsort [] = []
heapsort [a] = [a]
heapsort xs = internalSort [] (toHeapBTree xs)
    where
        internalSort [] Empty = []
        internalSort xs Empty = xs
        internalSort xs heap = internalSort (root heap:xs) (deleteRoot heap)

-- binary search tree
insertBinTree :: Ord a => BTree a -> a -> BTree a
insertBinTree Empty a = Leaf a
insertBinTree (Leaf a) v 
    | a >= v = Node (Leaf v) a  Empty
    | otherwise = Node Empty a (Leaf v)
insertBinTree (Node l v r) nv 
    | nv <= v = Node (insertBinTree l nv) v r
    | otherwise = Node Empty v (insertBinTree r nv)

createBinSearchTree :: Ord a => [a] -> BTree a
createBinSearchTree [] = Empty
createBinSearchTree a = internalCreate Empty a
    where
        internalCreate tree [] = tree
        internalCreate Empty (a:as) = internalCreate (insertBinTree Empty a) as
        internalCreate tree (a:as) = internalCreate (insertBinTree tree a) as

binTreeContains :: Ord a => BTree a -> a -> Bool
binTreeContains Empty key = False 
binTreeContains (Leaf a) key = a == key
binTreeContains node@(Node l v r) key
    | v == key = True
    | v > key = binTreeContains l key
    | v < key = binTreeContains r key


-- 4 Deph-First Search
dfs :: Ord a => BTree a -> a -> Bool
dfs Empty key = False
dfs (Leaf a) key = a == key
dfs tree@(Node l v r) key 
    | v == key = True
    | dfs l key = True
    | otherwise = dfs r key

main :: IO()
main = do
    -- 1. merge sort
    print $ mergeSort ([] :: [Int])
    print $ mergeSort [1]
    print $ mergeSort [1,2]
    print $ mergeSort [2,1]
    print $ mergeSort [2,1,3]
    print $ mergeSort [2,1,3,-10,15,33,99,42]

    print $ mergeSort ["Macaco", "Abacaxi"]
    print $ mergeSort ["Laranja", "Batatinha", "Ze"]
    
    -- 2. heapsort
    print $ heapsort ([] :: [Int])
    print $ heapsort [1]
    print $ heapsort [1,2]
    print $ heapsort [2,1]
    print $ heapsort [2,1,3]
    print $ heapsort [2,1,3,-10,15,33,99,42]

    print $ heapsort ["Macaco", "Abacaxi"]
    print $ heapsort ["Laranja", "Batatinha", "Ze"]

    -- 3. binary search tree
    let a = insertBinTree Empty 1
    let b = insertBinTree a 2
    let c = insertBinTree b 0
    
    print a
    print b
    print c
    print $ insertBinTree (insertBinTree b 4) 5
    print $ insertBinTree (insertBinTree (insertBinTree b 4) 5) 3
    print $ createBinSearchTree [1,2,0,4,5,3]
    print $ binTreeContains (createBinSearchTree [1,2,0,4,5,3]) 5
    print $ binTreeContains (createBinSearchTree [1,2,0,4,5,3]) 10

    print $ createBinSearchTree ["Batata", "Cachorro", "Limoeiro", "Mexico", "Cpp", "Microfone", "Abobrinha", "Cama"]

    -- 4. dfs
    print $ dfs (createBinSearchTree [1,2,0,4,5,3]) 5
    print $ dfs (createBinSearchTree [1,2,0,4,5,3]) 10


