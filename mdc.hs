module MDC where

mdc :: Int -> Int -> Int
mdc a 0 = abs a
mdc a b = mdc b (rem a b)

somaDig :: Int -> Int
somaDig val 
    | val < 10  = val
    | otherwise = mod val 10 + somaDig (div val 10)

isPal :: String -> Bool 
isPal "" = True
isPal [_] = True
isPal w = (head w == last w) && isPal (middle w)
    where
        middle = tail . init

meuReverse :: String -> String
meuReverse "" = ""
meuReverse (a:as) = meuReverse as ++ [a]