module Main where

f :: Int -> [Int] -> [Int]
f _ [] = []
f n [a] = replicate n a
f n arr = f n [head arr] ++ f n (tail arr)

main :: IO ()
main = undefined 