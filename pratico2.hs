module Main where

-- Alunos
-- Gabriel Przytocki
-- Matheus Bertho
-- Valdemar Ceccon

-- 1. Escreva uma função que devolva a soma de todos os números menores que 10000 que 
-- sejam múltiplos de 3 ou 5.  

somaMult3ou5A :: Int -> Int
somaMult3ou5A limit = sum (filter mult3ou5 [0..limit - 1])
    where
        mult5 n = rem n 5 == 0
        mult3 n = rem n 3 == 0
        mult3ou5 n = mult3 n || mult5 n

somaMult3ou5B :: Int -> Int
somaMult3ou5B 0 = 0
somaMult3ou5B limit = v + somaMult3ou5B (limit - 1)
    where
        v | rem (limit - 1) 3 == 0 = limit - 1
          | rem (limit - 1) 5 == 0 = limit - 1
          | otherwise = 0

-- 2. Escreva uma que devolva a diferença entre a soma de todos os números de Fibonacci 
-- ímpares  menores  que  100.000  e  a  soma  de  todos  os  números  de  Fibonacci  pares 
-- menores que 100.000. 
fibs :: [Int]
fibs = 1 : 1 : zipWith (+) fibs (tail fibs)

diffFibParImpar :: Int -> Int
diffFibParImpar limit = abs (sum pares - sum impares)
    where
        f = takeWhile (< limit) fibs
        pares = filter even f
        impares = filter odd f


-- 3. Fatorar um número em seus divisores primos é uma tarefa importante para a segurança 
-- de  dados.  Escreva  uma  função  que  devolva  os  fatores  primos  de  um  inteiro  dado. 
-- Observe que este inteiro pode ser maior que 100.000.000.  
primeFactors1 :: Int -> [Int]
primeFactors1 1 = []
primeFactors1 n | null divisibles = [n]
               | otherwise = head divisibles:primeFactors1(div n (head divisibles))
    where
        divisibles = filter (\a-> rem n a == 0) (2:[3,5..(n-1)])
    
    
-- primeFactor2 :: Int -> [Int]
-- primeFactor2  n = map snd $ filter (\(a,_) -> a>0) (zip (map (countDiv n) (2:[3,5..n-1])) (2:[3,5..n-1])) 
-- countDiv :: Int -> Int -> Int
-- countDiv 1 _ = 0
-- countDiv n f | rem n f == 0 = 1 + countDiv (div n f) f
--              | otherwise = 0

-- 4. Escreva uma função que, recebendo uma lista de inteiros, apresente a diferença entre 
-- a soma dos quadrados e o quadrado da soma destes inteiros. 
somaQQ1 :: [Integer] -> Integer
somaQQ1 li = abs $ sum (map (^2) li) - (sum li ^ 2)

-- 5. O Crivo de Eratóstenes não é o melhor algoritmo para encontrar números primos. Crie 
-- uma função que  implemente  o  Crivo  de  Euler  (Euler’s  Sieve) para  encontrar  todos  os 
-- números primos menores que um determinado inteiro dado.  
-- 6. Ignorando todas as variações da economia exceto o cálculo dos juros compostos escreva 
-- uma função que devolva uma lista infinita com todo o seu capital, em todos os meses 
-- que  se  seguem  de  hoje  até  o  fim  dos  tempos  considerando  que  será  passado  como 
-- argumento um valor de capital inicial, uma taxa de juros mensal e o número de um mês. 
-- Considere que seu investimento é tão bom que esta taxa será fixa até o final dos tempos. 
-- A  função  deverá  devolver  o  montante  em  um  determinado  mês  passado  de  forma 
-- cumulativa a partir de hoje de tal forma que se quiser saber o montante em um ano 
-- deverá passar 12 no argumento mês. 

invest :: Float -> Float -> Int -> Float
invest ini juros mes | mes == 0 = ini
                     | otherwise = ini + invest lucro juros (mes - 1)
                     where
                         lucro = ini + ini * juros

-- 7. Escreva  uma  função,  usando  interate  que  devolva  uma  lista  infinita  de  inteiros  de  tal 
-- forma que o inteiro n será o dobro do inteiro n-1. Esta função deve receber o valor inicial 
-- da lista.  
infinityDouble1 :: Int -> [Int]
infinityDouble1 = iterate (2*)

infinityDouble2 :: Int -> [Int]
infinityDouble2 n = n:n*2:map (*2) (tail (infinityDouble2 n))
-- 8. Escreva  uma  função  que  receba  uma  string  e  devolva  outro  string  com  as  vogais 
-- trocadas. De tal forma que: a será transformado em u; e será transformado em o; i não 
-- será transformado; o será transformado em e e u será transformado em a;

swapVowels1 :: String -> String
swapVowels1 = map t
    where
        t a | a == 'a' = 'u'
            | a == 'e' = 'o'
            | a == 'o' = 'e'
            | a == 'u' = 'a'
            | a == 'i' = 'i'
            | otherwise = a

vowels :: [Char]
vowels = "aeiou"

swapVowels2 :: String -> String
swapVowels2 "" = ""
swapVowels2 (c:cs) = tv c:swapVowels2 cs
    where
        mv = filter (\a->fst a == c) (zip vowels (reverse vowels))
        tv a | null mv = a
             | otherwise = (snd . head) mv
        
-- 9. Nem só de Fibonacci vivem os exemplos de recursão. Escreva uma função que devolva 
-- todos  os  números  de  uma  sequência  de  Lucas  (2, 1, 3, 4, 7, 11, 18, 29, 47, 76, 123) 
-- menores que um inteiro dado.  
lucas :: [Int]
lucas = 2:1:zipWith (+) lucas (tail lucas)

lucasN1 :: Int -> [Int]
lucasN1 n = takeWhile (<n) lucas


-- 10. Escreva  uma  função  análoga  a  função  map  na  qual  o  predicado  seja  aplicado  a  duas 
-- listas  e  não  a  apenas  uma.  Por  exemplo  map2 (+) [1,2,3] [10,11,12] deve  retornar 
-- [11,13,15].
map2_1 :: (a -> b -> c) -> [a] -> [b] -> [c]
map2_1 = zipWith

map2_2 :: (a -> b -> c) -> [a] -> [b] -> [c]
map2_2 fun la lb
    | length la /= length lb = error "Lists length must match"
    | null la = []
    | otherwise = fun (head la) (head lb):map2_2 fun (tail la) (tail lb)


main :: IO ()
main = do
    -- 1
    print (somaMult3ou5A 10000)
    print (somaMult3ou5B 10000)

    -- 2 
    -- 1,1,2,3,5,8,13,21,34,55,89
    -- pares 2 8 34 = 44
    -- impares 1 1 3 5 13 21 55 89 = 188
    -- diferenca 144
    print (diffFibParImpar 100)

    -- 3
    -- fatores primos de 10 = 2 5
    -- fatores primos de 20 = 2 2 5
    -- fatores primos de 30 = 2 3 5
    print (primeFactors1 10)
    print (primeFactors1 20)
    print (primeFactors1 30)

    -- 4 lista de inteiro [2, 3, 10]
    -- soma dos quadrados = 4 + 9 + 100 = 113
    -- quadrado das somas = 15 = 225
    -- diferença = 112
    print $ somaQQ1 [2,3,10]

    -- 7
    print $ take 20 $ infinityDouble1 5

    -- 8
    print $ swapVowels1 "valdemar"

    -- 9 
    print $ lucasN1 10

    -- 10
    print $ map2_2 (+) [1,2,3] [4,5,6]