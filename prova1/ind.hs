module Main where

-- TRABALHO 2 ÁREA DE POLINÔMIOS 
-- Até o final do Século XIX, o cálculo da área de polinômios era um problema não resolvido. Nesta 
-- época,  Meister  e  Gauss encontraram uma  forma  de resolver  este  problema  que  hoje é  chamada  de 
-- Fórmula  do  Cadarço  do  Sapato,  pelos  professores  menos  formais.  Você  pode  encontrar  um  texto 
-- interessante sobre isso em:  Shoelace formula - Wikipedia.  
-- Seu trabalho será implementar uma funcao em Haskell que calcule a área de um poligono, no 
-- plano, a partir de uma lista de pares ordenados contendo os vértices deste polígono.  
 
-- Frank Coelho de Alcantara 
-- Exercicios, pesquisas e atividades 
-- EXERCÍCIOS AVALIAcaO INDIVIDUAL 
-- Observe  que  não  há  nenhum  limite  para  o  números  de  lados  que  este  polígono  terá.  Você 
-- deverá encontrar duas soluções para este problema: 
-- 1.  Usando funções disponíveis no Prelude 


shoeLace1 :: [(Float,Float)] -> Float
shoeLace1 pares | length pares < 3 = 0.0
               | otherwise = (sum (map (uncurry (*)) s2Pares) - sum (map (uncurry (*)) s1Pares)) / 2.0
                where
                    (xs,ys) = unzip pares
                    s1Pares = zip xs (drop 1 (cycle ys))
                    s2Pares = zip (drop 1 (cycle xs)) ys



-- 2.  Não usando nenhuma funcao disponível no Prelude

meuFst :: (a,b) -> a
meuFst (a,b) = a

meuSnd :: (a,b) -> b
meuSnd (a,b) = b

meuSum :: (Num a) => [a] -> a
meuSum [] = 0
meuSum (a:b) = a + sum b

meuMap :: (a->b) -> [a] -> [b]
meuMap _ [] = []
meuMap f (x:xs) = f x : meuMap f xs

meuZip :: [a] -> [a] -> [(a,a)]
meuZip [] _ = []
meuZip _ [] = []
meuZip (l1h:l1s) (l2h:l2s) = (l1h,l2h):meuZip l1s l2s

meuUnzip :: [(a,a)] -> ([a],[a])
meuUnzip [] = ([],[])
meuUnzip ((a,b):xs) = (a:meuFst (meuUnzip xs), b:meuSnd (meuUnzip xs))

meuLength :: [a] -> Int
meuLength [] = 0
meuLength (_:xs) = 1 + meuLength xs

meuDrop :: Int -> [a] -> [a]
meuDrop _ [] = []
meuDrop 0 xs = xs
meuDrop n (_:xs) = xs

meuCycle :: [a] -> [a]
meuCycle xs = xs ++ meuCycle xs

shoeLace2 :: [(Float,Float)] -> Float
shoeLace2 pares | meuLength pares < 3 = 0.0
                | otherwise = (meuSum (meuMap (\a -> meuFst a * meuSnd a) s2Pares) - meuSum (meuMap (\a -> meuFst a * meuSnd a) s1Pares)) / 2.0
                where
                    (xs,ys) = meuUnzip pares
                    s1Pares = meuZip xs (meuDrop 1 (meuCycle ys))
                    s2Pares = meuZip (meuDrop 1 (meuCycle xs)) ys


-- TRABALHO 3 DEZ EXERCÍCIOS 
-- Sem utilizar nenhuma das funções disponíveis no Prelude escreva funções capazes de: 
-- a) Encontrar o penúltimo elemento de uma lista dada; 
beforeLast :: [a] -> a
beforeLast [] = error "Empty list"
beforeLast [a] = error "List too short"
beforeLast [a,b] = a
beforeLast (a:b) = beforeLast b

-- b) Encontrrar o elemento de índice k de uma lista dada;  
getEl :: Int -> [a] -> a
getEl 0 (n:_) = n
getEl _ [] = error "Invalid index"
getEl n (ns:r) = getEl (n-1) r

-- c) Determinar se uma determinada lista é um palíndromo;
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (a:as) = myReverse as ++ [a]

myZipWith :: (a -> a -> b) -> [a] -> [a] -> [b]
myZipWith _ [] [] = []
myZipWith _ [] (_:_) = error "Lists length does not match"
myZipWith _ (_:_) [] = error "Lists length does not match"
myZipWith fn (l1:l1s) (l2:l2s) = fn l1 l2 : myZipWith fn l1s l2s

myFoldl :: (a -> a -> a) -> a -> [a] -> a
myFoldl fn acc [] = acc
myFoldl fn acc (x:xs) = fn acc (myFoldl fn x xs)

isPalimdromo :: Eq a => [a] -> Bool
isPalimdromo ls = myFoldl (&&) True (myZipWith (==) ls (myReverse ls))

-- d) Duplicar os elementos de uma lista: com [1,2,3] teremos [1,1,2,2,3,3]; 
dupElementos :: [a] -> [a]
dupElementos [] = []
dupElementos (a:as) = a:a:dupElementos as

-- e) Remover o elemento de índice k de uma lista: com [0,0,0,1,0,0,0] teremos [0,0,0,0,0,0]; 
delElement :: (Eq a, Num a) => a -> [b] -> [b]
delElement _ [] = error "Invalid index"
delElement 0 (x:xs) = xs
delElement n (x:xs) = x:delElement (n-1) xs

-- f) Extrair os elementos entre os índices k e l de uma lista; 
subList :: (Eq a, Num a) => a -> a -> [b] -> [b]
subList 0 0 [] = []
subList _ _ [] = error "Invalid indexes"
subList 0 0 l = []
subList 0 high (x:xs) = x:subList 0 (high-1) xs
subList low high (x:xs) = subList (low-1) (high-1) xs

-- g) Inserir um elemento no índice k de uma lista, a lista aumentará um elemento; 
putElem :: (Eq b, Num b) => a -> b -> [a] -> [a]
putElem elem 0 [] = [elem]
putElem _ idx [] = error "Invalid Index"
putElem elem 0 list = elem:list
putElem elem idx (x:xs) = x:putElem elem (idx - 1) xs

-- h) Deslocar n elementos de uma lista a esquerda (rotacao a esquerda); 
rotLeft :: [a] -> [a]
rotLeft [] = []
rotLeft (a:as) = as ++ [a]

rotLeftN :: (Eq b, Num b) => b -> [a] -> [a]
rotLeftN 0 x = x
rotLeftN n x = rotLeftN (n-1) (rotLeft x)

-- i) Deslocar n elementos de uma lista a direita (rotacao a direita); 
rotRight :: [a] -> [a]
rotRight [] = []
rotRight xs = myReverse . rotLeft . myReverse $ xs

rotRightN :: (Eq b, Num b) => b -> [a] -> [a]
rotRightN 0 x = x
rotRightN n x = rotRightN (n-1) (rotRight x)

-- j) Transformar uma lista dada em uma lista de pares ordenados onde o primeiro elemento de 
-- cada para será um elemento de índice impar na lista original e o segundo elemento de cada 
-- para será um elemento de índice par na lista original.
listToTuples :: [a] -> [a]
listToTuples [] = []
listToTuples [_] = error "Wrong dimension"
listToTuples (a:b:xs) = b:a:listToTuples xs

main :: IO()
main = do
    print "Solution trabalho 2 1"
    print $ shoeLace1 [(3, 4), (5, 11), (12, 8), (9, 5) , (5, 6)]

    print "Solution trabalho 2 2"
    print $ shoeLace2 [(3, 4), (5, 11), (12, 8), (9, 5) , (5, 6)]

    print "Solution trabalho 3 a"
    print $ beforeLast "Valdemar"

    print "Solution trabalho 3 b"
    print $ getEl 2 "Valdemar"

    print "Solution trabalho 3 c"
    print "Is Ovo palindromo?"
    print $ isPalimdromo "ovo"
    print "Is Valdemar palindromo?"
    print $ isPalimdromo "valdemar"

    print "Solution trabalho 3 d"
    print $ dupElementos "Valdemar"

    print "Solution trabalho 3 e"
    print $ delElement 3 "Valdemar"

    print "Solution trabalho 3 f"
    print $ subList 1 4 "Valdemar"

    print "Solution trabalho 3 g"
    print $ map (\ n ->putElem 'z' n "Valdemar") [0..length "Valdemar"]
    
    print "Solution trabalho 3 h"
    print $ rotLeft "Valdemar"

    print "Solution trabalho 3 i"
    print $ rotRight "Valdemar"

    print "Solution trabalho 3 j"
    print $ listToTuples "Valdemar"

