module Main where

-- TRABALHO 3 DEZ EXERCÍCIOS 
-- Sem utilizar nenhuma das funções disponíveis no Prelude escreva funções capazes de: 
-- a) Encontrar o penúltimo elemento de uma lista dada; 
beforeLast :: [a] -> a
beforeLast [] = error "Empty list"
beforeLast [a] = error "List too short"
beforeLast [a,b] = a
beforeLast (a:b) = beforeLast b

-- b) Encontrrar o elemento de índice k de uma lista dada;  
getEl :: Int -> [a] -> a
getEl 0 (n:_) = n
getEl _ [] = error "Invalid index"
getEl n (ns:r) = getEl (n-1) r

-- c) Determinar se uma determinada lista é um palíndromo;
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (a:as) = myReverse as ++ [a]

myZipWith :: (a -> a -> b) -> [a] -> [a] -> [b]
myZipWith _ [] [] = []
myZipWith _ [] (_:_) = error "Lists length does not match"
myZipWith _ (_:_) [] = error "Lists length does not match"
myZipWith fn (l1:l1s) (l2:l2s) = fn l1 l2 : myZipWith fn l1s l2s

myFoldl :: (a -> a -> a) -> a -> [a] -> a
myFoldl fn acc [] = acc
myFoldl fn acc (x:xs) = fn acc (myFoldl fn x xs)

isPalimdromo :: Eq a => [a] -> Bool
isPalimdromo ls = myFoldl (&&) True (myZipWith (==) ls (myReverse ls))

-- d) Duplicar os elementos de uma lista: com [1,2,3] teremos [1,1,2,2,3,3]; 
dupElementos :: [a] -> [a]
dupElementos [] = []
dupElementos (a:as) = a:a:dupElementos as

-- e) Remover o elemento de índice k de uma lista: com [0,0,0,1,0,0,0] teremos [0,0,0,0,0,0]; 
delElement :: (Eq a, Num a) => a -> [b] -> [b]
delElement _ [] = error "Invalid index"
delElement 0 (x:xs) = xs
delElement n (x:xs) = x:delElement (n-1) xs

-- f) Extrair os elementos entre os índices k e l de uma lista; 
subList :: (Eq a, Num a) => a -> a -> [b] -> [b]
subList 0 0 [] = []
subList _ _ [] = error "Invalid indexes"
subList 0 0 l = []
subList 0 high (x:xs) = x:subList 0 (high-1) xs
subList low high (x:xs) = subList (low-1) (high-1) xs

-- g) Inserir um elemento no índice k de uma lista, a lista aumentará um elemento; 
putElem :: (Eq b, Num b) => a -> b -> [a] -> [a]
putElem elem 0 [] = [elem]
putElem _ idx [] = error "Invalid Index"
putElem elem 0 list = elem:list
putElem elem idx (x:xs) = x:putElem elem (idx - 1) xs

-- h) Deslocar n elementos de uma lista a esquerda (rotação a esquerda); 
rotLeft :: [a] -> [a]
rotLeft [] = []
rotLeft (a:as) = as ++ [a]

rotLeftN :: (Eq b, Num b) => b -> [a] -> [a]
rotLeftN 0 x = x
rotLeftN n x = rotLeftN (n-1) (rotLeft x)

-- i) Deslocar n elementos de uma lista a direita (rotação a direita); 
rotRight :: [a] -> [a]
rotRight [] = []
rotRight xs = myReverse . rotLeft . myReverse $ xs

rotRightN :: (Eq b, Num b) => b -> [a] -> [a]
rotRightN 0 x = x
rotRightN n x = rotRightN (n-1) (rotRight x)

-- j) Transformar uma lista dada em uma lista de pares ordenados onde o primeiro elemento de 
-- cada para será um elemento de índice impar na lista original e o segundo elemento de cada 
-- para será um elemento de índice par na lista original.
listToTuples :: [a] -> [a]
listToTuples [] = []
listToTuples [_] = error "Wrong dimension"
listToTuples (a:b:xs) = b:a:listToTuples xs

main :: IO ()
main = do
    -- g
    print (map (\ n ->putElem 'z' n "Valdemar") [0..length "Valdemar"])