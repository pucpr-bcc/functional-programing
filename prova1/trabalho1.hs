module Main where
allInner :: String -> Int -> String
allInner "" _ = ""
allInner ")" _ = ""
allInner ('(':xs) n = allInner xs (n+1)
allInner (')':xs) n = allInner xs (n-1)
allInner (_:xs) 0 = allInner xs 0

allInner (x:xs) n = x:allInner xs n
-- allInner xs n = ""

solve :: String -> Double 
solve "" = 0.0
solve xs | 
solve f@('(':xs) = 5.0
solve xs = head . foldl ff [] . words $ xs
    where
        ff (x:y:xs) "+" = (x+y):xs
        ff xs ns = read ns:xs

operation :: (Fractional a, Read a) => [a] -> [Char] -> [a]
operation (x:y:xs) "+" = (x+y):xs
operation (x:y:xs) "*" = (x*y):xs
operation (x:y:xs) "-" = (x-y):xs
operation (x:y:xs) "/" = (x/y):xs
operation xs ns = read ns:xs


main :: IO()
main = do
    print $ solve "1 1 1 + + +"