module Main where
import Data.List (sort)

-- 1. Escreva uma função chamada soma1 que recebe um inteiro como argumento e retorna 
-- um inteiro uma unidade maior que a entrada.
soma1 :: Int -> Int
soma1 = (+ 1)

-- 2. Escreva uma função chamada sempre que, não importando o valor de entrada, devolva 
-- sempre zero. Observe que neste caso a entrada pode ser de qualquer tipo. 
sempre :: a -> Int
sempre _ = 0

-- 3. Escreva  uma  função  chamada  de  subi que  receba  dois  inteiros  e  devolva  o  valor  que 
-- resulta da operação primeiro valor menos segundo valor. 
subi :: Int -> Int -> Int
subi = (-)

-- 4. Escreva uma função chamada treco que receba três valores em ponto flutuantes com 
-- precisão  dupla  e  retorne  o  resultado  da  soma  dos  dois  primeiros  multiplicado  pelo 
-- terceiro.
treco :: Double -> Double -> Double -> Double
treco a b c = (a + b) * c

-- 5. Escreva  uma  função  chamada  contaDecrescente  que  recebe  uma  lista  de  inteiros  e 
-- devolve uma string com estes inteiros ordenados de forma decrescente. 
contaDecrescente :: [Int] -> String
contaDecrescente = unwords . map show . reverse . sort

-- 6. Escreva uma função chamada somaRecursiva que recebe dois valores inteiros e devolve 
-- o  produto  destes  valores  sem  usar  o  operador  de  multiplicação,  considerando  que  a 
-- operação produto pode ser definida como: 
somaRecursiva :: Int -> Int -> Int
somaRecursiva a b
    | b == 0 = 0
    | otherwise = a + somaRecursiva a (b-1)

-- 7. Escreva uma função chamada comprimento que receba uma lista de inteiros e devolva 
-- o  comprimento  desta  lista.  Observe que  você  não  pode usar nenhuma função que  já 
-- calcule o comprimento de uma lista. 
comprimento :: [a] -> Int
comprimento [] = 0
comprimento (_:xs) = 1 + comprimento xs

-- 8. Escreva uma função chamada reverso que receba uma lista e devolva esta lista com os 
-- elementos em ordem invertida. Observe que a lista recebida pode conter elementos de 
-- qualquer tipo.
reverso :: [a] -> [a]
reverso [] = []
reverso (a:as) = reverso as ++ [a]

-- 9. Escrevendo  suas próprias  funções  escreva  funções  alternativas para  head, tail, sum e 
-- last,  usando  obrigatoriamente  a  função  foldr.  Observe  que  todas estas  funções estão 
-- disponíveis no módulo Prelude e portanto, não poderão ser importadas. 
meuHead :: [Int] -> Maybe Int
meuHead [] = Nothing 
meuHead (a:_) = Just a

meuTail :: [Int] -> Maybe [Int]
meuTail [] = Nothing
meuTail (_:a) = Just a

meuSum :: [Int] -> Int
meuSum = foldr (+) 0

meuLast :: [Int] -> Maybe Int
meuLast [] = Nothing
meuLast a = meuHead (reverso a)

-- 10. Escreva uma função que recebe uma lista e troca a ordem dos seus elementos em pares. 
-- Por exemplo: [1, 2, 3, 4, 5] -> [2,1,4,3,5] assuma que a lista pode ter comprimento par, 
-- ou ímpar de elementos.
troca2 :: [Int] -> [Int]
troca2 [] = []
troca2 [a] = [a]
troca2 (a:b:xs) = [b, a] ++ troca2 xs

main :: IO()
main = do
    -- problema 1
    print $ soma1 10

    -- problema 2
    print $ sempre 10

    -- problema 3
    print $ subi 10 3

    -- problema 4
    print $ treco 1 2 3

    --problema 5
    print $ contaDecrescente [1,2,3,4,5]
    print $ contaDecrescente [1,-2,30,10,5]

    -- problema 6
    print $ somaRecursiva 2 10

    -- problema 7
    print $ comprimento [1,2,3,4]
    print $ comprimento "ab"
    print $ comprimento ""

    -- problema 8
    print $ reverso [1,2,3,4]
    print $ reverso "abc"
    print $ reverso ""

    -- problema 9
    print $ meuHead [1,2,3,4]
    print $ meuHead []

    print $ meuTail [1,2,3,4]
    print $ meuTail []

    print $ meuSum [1,2,3,4]
    print $ meuSum []

    print $ meuLast [1,2,3,4, -10]
    print $ meuLast []

    -- problema 10
    print $ troca2 [1,2,3,4,5]
    print $ troca2 [1,2,3,4]
    print $ troca2 [] 