module Main where

fizzbuzz :: Int -> [String]
fizzbuzz top = map fizzbuzzer [1..top]
    where
        fizzbuzzer a 
            | a `mod` 3 == 0 && a `mod` 5 == 0 = "FizzBuzz"
            | a `mod` 3 == 0 = "Fizz"
            | a `mod` 5 == 0 = "Buzz"
            | otherwise = show a

main :: IO()
main = putStrLn (unlines (fizzbuzz 100))