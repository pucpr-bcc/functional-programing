module Main where

data Pizza = Pizza { raio :: Float
                   , valor :: Float
                   } deriving (Eq, Show)

instance Ord Pizza where
    compare (Pizza ra ca) (Pizza rb cb) = compare (ra/ca) (rb/cb)

divisores :: Int -> [Int]
divisores a = [b | b <- [1..a-1], rem a b == 0 ]

conta :: Char -> String -> Int
conta c s = sum [1 | cs <- s, cs == c ]

dobraPositivo :: [Int] -> [Int]
dobraPositivo l = [ if x < 0 then x else x*2 | x <- l]

perfeitos :: Int -> [Int]
perfeitos n = [z | z <- [1..n], (sum . divisores) z == z]

triangulos :: Int -> [(Int,Int,Int)]
triangulos n = [(a,b,c) | a <- [1..n], b <- [a..n], c <- [b..n], (a + b) > c && (b + c) > a && (c + a) > b]

produtoEscalar :: [Int] -> [Int] -> Int
produtoEscalar a b = sum [uncurry (*) x | x <- zip a b]

primos :: Int -> [Int]
primos n = take n [x | x <- 2:[3,5..], ]

main :: IO()
main = do
    print $ divisores 10 
    print $ dobraPositivo [1,2,-3,10,-20]
    print $ triangulos 10
    print $ produtoEscalar [1,2,3] [1,2,3] 